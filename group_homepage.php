<?php
	session_start();
	include('includes/header.php');
	if(!isset($_SESSION['user_email'])){
		header("location: index.php");
	}
?>
<html>
	<head>
		<?php
		$user = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$user'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
		$user_name = $row['user_name'];
		$u_id = $_GET['u_id'];
		$g_n = $_GET['g_n'];
		
		$q = "select * from creategroup h inner join (select gid from groupmem where user_id = '$u_id') k on k.gid = h.gid where grp_name = '$g_n'";
		
		$r = mysqli_query($con, $q);
		$f = mysqli_fetch_array($r);
		
		$gid = $f['gid'];
		$g_name = $f['grp_name'];
		$g_info = $f['grp_info'];
		$g_img = $f['grp_img'];
		$cre = $f['date'];
		
		?>
		<title><?php echo "$user_name";?></title>
		<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
<link rel="stylesheet" type="text/css" href="style/home_style2.css"></link>
	</head>
	<style>
	body{
	overflow-x:hidden;
}
#cover-img{
	height:400px;
   	width:100%;
}
#profile_img{
	position: absolute;
	top: 160px;
	left: 40px;
}
#update_profile{
  	position: absolute;
  	top: 150px;
  	cursor: pointer;
  	left: 93px;
  	border-radius: 4px;
  	background-color: rgba(0,0,0,0.1);
  	transform: translate(-50%, -50%);
}
#button_profile{
  	position: absolute;
  	top: 108%;
  	left: 50%;
  	cursor: pointer;
  	transform: translate(-50%, -50%);
}
#upload_image_button{
	position: absolute;
	top: 48%;
	right: 13.4%;
	min-width: 100px;
	max-width: 100px;
	border-radius: 4px;
	transform: translate(-50%, -50%);
}
#abutton {
	position:relative;
	background-color: #ffffff;
	color:#000000;
	height: 35px;
	width: 150px;
	top:-18px;
	left:250px;
	border-radius: 4px;
	transform: translate(-50%, -50%);
 	
}


	</style>
	<body>
		<div class="row">
			<div class='col-sm-2'>
			</div>
			<div class='col-sm-8'>
			<?php
          echo "
         	<div>
				<div><img id='cover-img' class='img-rounded' src='groups_profile/$g_img' alt='group_image'/></div>
				<form action='group_homepage.php?u_id=$user_id&g_n=$g_name' method='post' enctype='multipart/form-data'>
					<ul class='nav pull-left' style='position: absolute;top: 10px;left: 40px;'>
				    	<li class='dropdown'>
				        	<button class='dropdown-toggle btn btn-default' data-toggle='dropdown'>Change Group image</button>
				        	
				        
				        	
				        	<div class='dropdown-menu'>
				        		<center>
				        		<p> Click <strong>Select Image</strong> and then click the <br> <strong>Update Image</strong></p>
				            	<label class='btn btn-info'> Select Image
						        <input type='file' name='u_cover' size='60' />
						        </label><br><br>
				                <button name='submit' class='btn btn-info'>Update Image</button>
				      
				            	</center>
				            </div>
				        </li>
				        <li>
				       <form> <a id='abutton' href='addfrd.php?u_id=$u_id&gid=$gid'>Add Friends</a></form>
				        
				        </li>
				    </ul>
	          	</form>
	          	
			</div>
         	<br>
         	
          "; ?>
<?php 

            if(isset($_POST['submit'])){

              $u_cover = $_FILES['u_cover']['name'];
              $image_tmp = $_FILES['u_cover']['tmp_name'];
              $random_number = rand(1,99999);

              if($u_cover==''){
                echo "<script>alert('Please Select Image!')</script>";
                echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name','_self')</script>";
                exit();
              }else{
              
              move_uploaded_file($image_tmp,"groups_profile/$u_cover.$random_number");
              
              $update = "update creategroup set grp_img='$u_cover.$random_number' where gid='$gid'";
              
              $run = mysqli_query($con,$update); 
              
              if($run){
              
              echo "<script>alert('Your Group Image Updated!')</script>";
              echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name','_self')</script>";
              }
            }
            
            }


          ?>
          
          
          </div>
 <div class="col-sm-2">
	</div>
</div>
		
			<div class='row'>
			<div class='col-sm-2'>
			</div>	
			<div id="insert_post" class="col-sm-8">
			<center>
			<form action="group_homepage.php?u_id=<?php echo $user_id;?>&g_n=<?php echo $g_name;?>" method="post" id="f" enctype="multipart/form-data">
			<textarea class="form-control" id="content" rows="4" name="content" placeholder="What is in your mind?"></textarea><br>
			<label class="btn btn-warning" id="upload_image_button">Select Image
			<input type="file" name="upload_image" size="30">
			</label>
			<button id="btn-post" class="btn btn-success" name="sub1">Post</button>
			</form>
			
			<?php insertgpost($gid,$g_name);?>
			
			</center>
			</div>
			<div class='col-sm-2'>
			</div>
		</div>
	<div class="row">
	<div class="col-sm-12">
		<center><h2><strong>Group Posts</strong></h2><br></center>
		<?php echo show_grpposts($gid); ?>
	</div>
	</div>	
	</body>
</html>
