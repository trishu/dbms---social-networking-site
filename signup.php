<html>
	<head>
		<title>SignUp</title>
		<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	</head>
	<style>
	body{
		overflow-x:hidden;
	}
	.main-content{
		width:50%;
		height:100%;
		margin:10px auto;
		background-color: #fff;
		border: 2px solid #e6e6e6;
		padding: 40px 50px;
	}
	.header{
		border:0px solid #000;
		margin-bottom:5px;
	}
	.well{
		background-color: #187fab;
	}
	#signup{
		width:60%:
		border-radius:30px;
	}
	</style>
	<body>
		<div class="row">
			<div class="col-sm-12">
				<div class="well">
					<center><h1 style="color:white;">Trial Network!</h1></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="main-content">
					<div class="header">
						<h3 style="text-align:center;"><strong>Join This Today!</strong></h3><br>
					</div>
					<div class="l-part">
					<form method="post" action="">
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
<input type="text" name="fname" class="form-control" placeholder="First Name" required="required">
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
<input type="text" name="lname" class="form-control" placeholder="Last Name" required="required">
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
<input id="password" type="password" name="u_pass" class="form-control" placeholder="Password" required="required">
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
<input id="email" type="email" name="u_email" class="form-control" placeholder="E-mail" required="required">
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
				<select class="form-control" name="u_country" required="required">
				<option>select your country</option>
				<option>India</option>
				<option>USA</option>
				<option>Japan</option>
				<option>France</option>
				<option>Germany</option>
				</select>
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-chevron-down"></i></span>
		<select class="form-control input-md" name="u_gender" required="required">
				<option>select your gender</option>
				<option>female</option>
				<option>male</option>
				<option>other</option>
				</select>
		</div><br>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
		<input type="date" name="u_birthday" class="form-control input-md"  required="required">
		</div><br>
		<a href="signin.php" style="text-decoration:none;float:right;color:#187fab;" data-toggle="tooltip" title="SignIn">Already have account?</a><br><br>
	<center><button id="signup" class="btn btn-info btn-lg" name="sign_up">SignUp</button></center>
						<?php
							include('insert_user.php');
						?> 
						</form>
				
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
