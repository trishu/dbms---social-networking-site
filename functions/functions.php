<?php

include('includes/connection.php');


function insertPost(){
	if(isset($_POST['sub'])){
		global $con;
		global $user_id;

		$content = htmlentities($_POST['content']);
		$upload_image = $_FILES['upload_image']['name'];
		$image_tmp = $_FILES['upload_image']['tmp_name'];
		$random_number = rand(1, 100);

		if(strlen($content) > 250){
			echo "<script>alert('Please Use 250 or less than 250 words!')</script>";
			echo "<script>window.open('home.php', '_self')</script>";
		}else{
			if(strlen($upload_image) >= 1 && strlen($content) >= 1){
				move_uploaded_file($image_tmp, "imagepost/$upload_image.$random_number");
				$insert = "insert into posts (user_id, post_content, upload_image, post_date) values('$user_id', '$content', '$upload_image.$random_number', NOW())";

				$run = mysqli_query($con, $insert);

				if($run){
					echo "<script>alert('Your Post updated a moment ago!')</script>";
					echo "<script>window.open('home.php', '_self')</script>";

					$update = "update users set posts='yes' where user_id='$user_id'";
					$run_update = mysqli_query($con, $update);
				}

				exit();
			}
			else{
				if($upload_image=='' && $content == ''){
					echo "<script>alert('Error Occured while uploading!')</script>";
					echo "<script>window.open('home.php', '_self')</script>";
				}else{
					if($content==''){
						move_uploaded_file($image_tmp, "imagepost/$upload_image.$random_number");
						$insert = "insert into posts (user_id,post_content,upload_image,post_date) values ('$user_id','No','$upload_image.$random_number',NOW())";
						$run = mysqli_query($con, $insert);

						if($run){
							echo "<script>alert('Your Post updated a moment ago!')</script>";
							echo "<script>window.open('home.php', '_self')</script>";

							$update = "update users set posts='yes' where user_id='$user_id'";
							$run_update = mysqli_query($con, $update);
						}

						exit();
					}
					else{
					
						$insert = "insert into posts (user_id, post_content, post_date) values('$user_id', '$content', NOW())";
						$run = mysqli_query($con, $insert);

						if($run){
							echo "<script>alert('Your comment updated a moment ago!')</script>";
							echo "<script>window.open('home.php', '_self')</script>";

							$update = "update users set posts='yes' where user_id='$user_id'";
							$run_update = mysqli_query($con, $update);
						}
					}
				}
			}
		}
	}
}

function get_posts() {
	global $con;
	$per_page = 4;

	if(isset($_GET['page'])){
		$page = $_GET['page'];
	}else{
		$page=1;
	}

	$start_from = ($page-1) * $per_page;

	$get_posts = "select * from posts ORDER by 1 DESC LIMIT $start_from, $per_page";
	
	$run_posts = mysqli_query($con, $get_posts);
	
	echo mysqli_num_rows($run_posts);

	while($row_posts = mysqli_fetch_array($run_posts)) {

		$post_id = $row_posts['post_id'];
		$user_id = $row_posts['user_id'];
		$content = substr($row_posts['post_content'], 0 ,250);
		$upload_image = $row_posts['upload_image'];
		$post_date = $row_posts['post_date'];

		$user = "select *from users where user_id='$user_id' AND posts='yes'";
		$run_user = mysqli_query($con,$user);
		$row_user = mysqli_fetch_array($run_user);

		$user_name = $row_user['user_name'];
		$user_image = $row_user['user_image'];

		$like = "select * from postlikestatus where post_id = '$post_id' AND likestatus = '1'";
		$run_like = mysqli_query($con, $like);		
		
		$no_of_likes = mysqli_num_rows($run_like);
		
		$comment1 = "select * from comments where post_id = '$post_id'";
		$com_run = mysqli_query($con, $comment1);
		
		$no_of_comments = mysqli_num_rows($com_run);

		//echo $no_of_likes;
		//$row_like = mysqli_fetch_array($run_like);
		
		
		if($content=="No" && strlen($upload_image) >= 1){
			echo"
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
					<div class='row'>
						<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
						</div>
						<div class='col-sm-6'>
							<h3><a style='text-decoration:none; cursor:pointer;color #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
							<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
						</div>
						<div class='col-sm-4'>
						</div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
						</div>
					</div><br>
					<a href='' style='float:right;'><form method='post' action=''><button name= 'unlike1' class='btn btn-warning'>Unlike</button>			<input type='hidden' name='upos1' value='$post_id' />
</form></a>	
					<a href='' style='float:right;'><form method='post' action=''><button name= 'like1' class='btn btn-success'>Like($no_of_likes)</button>			<input type='hidden' name='pos1' value='$post_id' />
</form></a>
					<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment($no_of_comments)</button></a>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";
			
				if(isset($_POST['like1']) && isset($_POST['pos1'])){

			
			$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
		
		$pos_id = $_POST['pos1'];	
		$ui_id = $row['user_id'];	

			$insert = "insert into postlikestatus (post_id, user_id, likestatus) values ('$pos_id','$ui_id','1')";

			$run = mysqli_query($con,$insert);
			
			if($run == false){
				echo "<script>alert('you have already liked this post')</script>";
				//continue;
				echo "<script>window.open('home.php', '_self')</scirpt>";
			
			}
			else{	
				echo"<script>alert('you liked this post')</script>";
				//continue;
				echo "<script>window.open('home.php','_self')</script>";
			}
			}

			if(isset($_POST['unlike1']) && isset($_POST['upos1'])){

			
		$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
		
		$pos_id = $_POST['upos1'];	
		$ui_id = $row['user_id'];	

			$n1 = "select * from postlikestatus";
			$r1 = mysqli_query($con, $n1);
			
			$a1 = mysqli_num_rows($r1);
			$del = "delete from postlikestatus where post_id = '$pos_id' AND user_id = '$ui_id'";

			$run = mysqli_query($con,$del);
			
			$n2 = "select * from postlikestatus";
			$r2 = mysqli_query($con, $n2);
			
			$a2 = mysqli_num_rows($r2);
			if($a1 == $a2){
				echo "<script>alert('Not able to unlike unless you like the post')</script>";
				//continue;
				echo "<script>window.open('home.php', '_self')</scirpt>";
			}
			else{	
				echo"<script>alert('you have unliked this post')</script>";
				//continue;
				echo "<script>window.open('home.php','_self')</script>";
			}
			}
		

		}

		else if(strlen($content) >= 1 && strlen($upload_image) >= 1){
			echo"
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
					<div class='row'>
						<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
						</div>
						<div class='col-sm-6'>
							<h3><a style='text-decoration:none; cursor:pointer;color #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
							<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
						</div>
						<div class='col-sm-4'>
						</div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<p>$content</p>
							<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
						</div>
					</div><br>
				<a href='' style='float:right;'><form method='post' action=''><button name= 'unlike2' class='btn btn-warning'>Unlike</button>			<input type='hidden' name='upos2' value='$post_id' />
</form></a>	
				<a href='' style='float:right;'><form method='post' action=''><button class='btn btn-success' name = 'like2'>Like($no_of_likes)</button>			<input type='hidden' name='pos2' value='$post_id' />
</form></a>

					<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment($no_of_comments)</button></a>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";
			if(isset($_POST['like2']) && isset($_POST['pos2'])){

		$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
			
		$ui_id = $row['user_id'];	
		$pos_id = $_POST['pos2'];
			
			$insert = "insert into postlikestatus (post_id, user_id, likestatus) values ('$pos_id','$ui_id','1')";

			$run = mysqli_query($con,$insert);
			
			if($run == false){
				echo "<script>alert('you have already liked this post')</script>";
				echo "<script>window.open('home.php', '_self')</scirpt>";
			}
			else{	
				echo"<script>alert('you liked this post')</script>";
				echo "<script>window.open('home.php','_self')</script>";
			}
			}
		if(isset($_POST['unlike2']) && isset($_POST['upos2'])){

			
		$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
		
		$pos_id = $_POST['upos2'];	
		$ui_id = $row['user_id'];	

			$n1 = "select * from postlikestatus";
			$r1 = mysqli_query($con, $n1);
			
			$a1 = mysqli_num_rows($r1);
			$del = "delete from postlikestatus where post_id = '$pos_id' AND user_id = '$ui_id'";

			$run = mysqli_query($con,$del);
			
			$n2 = "select * from postlikestatus";
			$r2 = mysqli_query($con, $n2);
			
			$a2 = mysqli_num_rows($r2);
			if($a1 == $a2){
				echo "<script>alert('Not able to unlike unless you like the post')</script>";
				echo "<script>window.open('home.php', '_self')</scirpt>";
			}
			else{	
				echo"<script>alert('you have unliked this post')</script>";
				echo "<script>window.open('home.php','_self')</script>";
			}
			}


		}

		else{
			echo"
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
					<div class='row'>
						<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
						</div>
						<div class='col-sm-6'>
							<h3><a style='text-decoration:none; cursor:pointer;color #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
							<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
						</div>
						<div class='col-sm-4'>
						</div>
					</div>
					<div class='row'>
						<div class='col-sm-12'>
							<h3><p>$content</p></h3>
						</div>
					</div><br>
					<a href='' style='float:right;'><form method='post' action=''><button name= 'unlike3' class='btn btn-warning'>Unlike</button>			<input type='hidden' name='upos3' value='$post_id' />
</form></a>	
					<a href='' style='float:right;'><form method='post' action=''><button class='btn btn-success' name = 'like3'>Like($no_of_likes)</button>
					<input type='hidden' name='pos3' value='$post_id' />
</form></a>
					<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment($no_of_comments)</button></a>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";
			if(isset($_POST['like3']) && isset($_POST['pos3'])){
			$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
			
		$ui_id = $row['user_id'];	
		$pos_id = $_POST['pos3'];
			$insert = "insert into postlikestatus (post_id, user_id, likestatus) values ('$pos_id','$ui_id','1')";

			$run = mysqli_query($con,$insert);
			
			if($run == false){
				echo "<script>alert('you have already liked this post')</script>";
				echo "<script>window.open('home.php', '_self')</scirpt>";
			}
			else{	
				echo"<script>alert('you liked this post')</script>";
				echo "<script>window.open('home.php','_self')</script>";
			}
			}
			
		if(isset($_POST['unlike3']) && isset($_POST['upos3'])){

			
		$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
		
		$pos_id = $_POST['upos3'];	
		$ui_id = $row['user_id'];	

			$n1 = "select * from postlikestatus";
			$r1 = mysqli_query($con, $n1);
			
			$a1 = mysqli_num_rows($r1);
			$del = "delete from postlikestatus where post_id = '$pos_id' AND user_id = '$ui_id'";

			$run = mysqli_query($con,$del);
			
			$n2 = "select * from postlikestatus";
			$r2 = mysqli_query($con, $n2);
			
			$a2 = mysqli_num_rows($r2);
			if($a1 == $a2){
				echo "<script>alert('Not able to unlike unless you like the post')</script>";
				echo "<script>window.open('home.php', '_self')</scirpt>";
			}
			else{	
				echo"<script>alert('you have unliked this post')</script>";
				echo "<script>window.open('home.php','_self')</script>";
			}
			}

			

		}
	}

	include("pagination.php");
  } 
  
function single_post(){

	if(isset($_GET['post_id'])){

	global $con;

	$get_id = $_GET['post_id'];

	$get_posts = "select * from posts where post_id='$get_id'";

	$run_posts = mysqli_query($con,$get_posts);

	$row_posts=mysqli_fetch_array($run_posts);

		$post_id = $row_posts['post_id'];
		$user_id = $row_posts['user_id'];
		$content = $row_posts['post_content'];
		$upload_image = $row_posts['upload_image'];
		$post_date = $row_posts['post_date'];

		//getting the user who has posted the thread
		$user = "select * from users where user_id='$user_id' AND posts='yes'";

		$run_user = mysqli_query($con,$user);
		$row_user=mysqli_fetch_array($run_user);

		$user_name = $row_user['user_name'];
		$user_image = $row_user['user_image'];

		// getting the user session
		$user_com = $_SESSION['user_email'];

		$get_com = "select * from users where user_email='$user_com'";
		$run_com = mysqli_query($con,$get_com);
		$row_com=mysqli_fetch_array($run_com);

		$user_com_id = $row_com['user_id'];
		$user_com_name = $row_com['user_name'];


		//now displaying all at once



		if(isset($_GET['post_id'])){
			$post_id = $_GET['post_id'];
			}
			$get_posts = "select post_id from users where post_id='$post_id'";
			$run_user = mysqli_query($con,$get_posts);

			$post_id = $_GET['post_id'];

			$post = $_GET['post_id'];
			$get_user = "select * from posts where post_id='$post'";
			$run_user = mysqli_query($con,$get_user);
			$row=mysqli_fetch_array($run_user);

			$p_id = $row['post_id'];

			if($p_id != $post_id){
				echo "<script>alert('ERROR')</script>";
				echo "<script>window.open('home.php','_self')</script>";
			}else{


		if($content=="No" && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else if(strlen($content) >= 1 && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<p>$content</p>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else{

		echo "

		<div class='row'>
			<div class='col-sm-3'>
			</div>
			<div id='posts' class='col-sm-6'>
			<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-2'>
					</div>
					<div class='col-sm-6'>
						<h3><p>$content</p></h3>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				
			</div>
			<div class='col-sm-3'>
			</div>
		</div><br><br>

		";
	}
		include("comments.php");

		echo "
		<div class='row'>
        <div class='col-md-6 col-md-offset-3'>
            <div class='panel panel-info'>
                <div class='panel-body'>
                	<form action='' method='post' class='form-inline'>
                    <textarea placeholder='Write your comment here!'' class='pb-cmnt-textarea' name='comment'></textarea>
                    <button class='btn btn-info pull-right' name='reply'>Comment</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
		";

		if(isset($_POST['reply'])){

			$comment = htmlentities($_POST['comment']);

			if($comment == ""){
			echo"<script>alert('Enter your comment!')</script>";
			echo "<script>window.open('single.php?post_id=$post_id','_self')</script>";
			}else{
			$insert = "insert into comments (post_id,user_id,comment,comment_author,date) values ('$post_id','$user_id','$comment','$user_com_name',NOW())";

			$run = mysqli_query($con,$insert);

			echo"<script>alert('Your Reply was added!')</script>";
			echo "<script>window.open('single.php?post_id=$post_id','_self')</script>";
		}

		}

	}
	}

	}
function user_posts(){


	global $con;

			if(isset($_GET['u_id'])){
			$u_id = $_GET['u_id'];
			}
			$get_posts = "select * from posts where user_id='$u_id' ORDER by 1 DESC LIMIT 5";

			$run_posts = mysqli_query($con,$get_posts);

			while($row_posts=mysqli_fetch_array($run_posts)){

			$post_id = $row_posts['post_id'];
			$user_id = $row_posts['user_id'];
			$content = $row_posts['post_content'];
			$upload_image = $row_posts['upload_image'];
			$post_date = $row_posts['post_date'];


			$user = "select * from users where user_id='$user_id' AND posts='yes'";

			$run_user = mysqli_query($con,$user);
			$row_user=mysqli_fetch_array($run_user);

			$user_name = $row_user['user_name'];
			$user_image = $row_user['user_image'];

			if(isset($_GET['u_id'])){
			$u_id = $_GET['u_id'];
			}
			$get_posts = "select user_email from users where user_id='$u_id'";
			$run_user = mysqli_query($con,$get_posts);
			$row=mysqli_fetch_array($run_user);

			$user_email = $row['user_email'];

			$user = $_SESSION['user_email'];
			$get_user = "select * from users where user_email='$user'";
			$run_user = mysqli_query($con,$get_user);
			$row=mysqli_fetch_array($run_user);

			$user_id = $row['user_id'];
			$u_email = $row['user_email'];

			if($u_email != $user_email){
				echo"<script>window.open('my_post.php?u_id=$user_id','_self')</script>";
			}else{

			if($content=="No" && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else if(strlen($content) >= 1 && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<p>$content</p>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else{

		echo "

		<div class='row'>
			<div class='col-sm-3'>
			</div>
			<div id='posts' class='col-sm-6'>
			<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-2'>
					</div>
					<div class='col-sm-6'>
						<h3><p>$content</p></h3>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
			</div>
			<div class='col-sm-3'>
			</div>
		</div><br><br>

		";
	}
			include("functions/delete_post.php");
		}

		}






	}
function results(){

	global $con;

	if(isset($_GET['search'])){
	$search_query = htmlentities($_GET['user_query']);
	}
	$get_posts = "select * from posts where post_content like '%$search_query%' OR upload_image like '%$search_query%'";

	$run_posts = mysqli_query($con,$get_posts);

	while($row_posts=mysqli_fetch_array($run_posts)){

		$post_id = $row_posts['post_id'];
		$user_id = $row_posts['user_id'];
		$content = substr($row_posts['post_content'],0,40);
		$upload_image = $row_posts['upload_image'];
		$post_date = $row_posts['post_date'];


		$user = "select * from users where user_id='$user_id' AND posts='yes'";

		$run_user = mysqli_query($con,$user);
		$row_user=mysqli_fetch_array($run_user);

		$user_name = $row_user['user_name'];
		$first_name = $row_user['f_name'];
		$last_name = $row_user['l_name'];
		$user_image = $row_user['user_image'];

		if($content=="No" && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else if(strlen($content) >= 1 && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<p>$content</p>
						<img id='posts-img' src='imagepost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else{

		echo "

		<div class='row'>
			<div class='col-sm-3'>
			</div>
			<div id='posts' class='col-sm-6'>
			<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-2'>
					</div>
					<div class='col-sm-6'>
						<h3><p>$content</p></h3>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<a href='single.php?post_id=$post_id' style='float:right;'><button class='btn btn-info'>Comment</button></a><br>
			</div>
			<div class='col-sm-3'>
			</div>
		</div><br><br>

		";
	}

	}
	}



function search_user(){

	global $con;

	if(isset($_GET['search_user_btn'])){
	$search_query = htmlentities($_GET['search_user']);
	$get_user = "select * from users where f_name like '%$search_query%' OR l_name like '%$search_query%' OR user_name like '%$search_query%'";
	}
	else{
	$get_user = "select * from users";
	}

	$run_user = mysqli_query($con,$get_user);

	while($row_user=mysqli_fetch_array($run_user)){

		$user_id = $row_user['user_id'];
		$f_name = $row_user['f_name'];
		$l_name = $row_user['l_name'];
		$username = $row_user['user_name'];
		$user_image = $row_user['user_image'];

		//now displaying all at once

		echo "
		<div class='row'>
			<div class='col-sm-3'>
			</div>

			<div class='col-sm-6'>

			<div class='row' id='find_people'>
			<div class='col-sm-4'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>
			<img class='img-circle' src='users/$user_image' width='150px' height='140px' title='$username' style='float:left; margin:1px;'/>
			</a>
			</div><br><br>
			<div class='col-sm-6'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>
			<strong><h2>$f_name $l_name</h2></strong>
			</a>
			</div>
			<div class='col-sm-3'>
			</div>

			</div>

			</div>
			<div class='col-sm-4'>
			</div>
		</div><br>
		";

	}

	}
	
	function search_groups(){

	global $con;
		$use = $_SESSION['user_email'];
		$get_user = "select * from users where user_email ='$use'";
		$run_user = mysqli_query($con, $get_user);
		$row = mysqli_fetch_array($run_user);
			
		$ui_id = $row['user_id'];	

	if(isset($_GET['search_user_bt'])){
	$search_query = htmlentities($_GET['search_user']);
	$get_user = "select groupmem.gid as g_id, grp_name, grp_info, grp_img, date from groupmem, creategroup where grp_name like '%$search_query%' OR grp_info like '%$search_query%' AND groupmem.user_id = '$ui_id'";
	}
	else{
	$get_user = "select * from creategroup h inner join (select gid from groupmem where user_id = '$ui_id') k on k.gid = h.gid;";
	}

	$run_user = mysqli_query($con,$get_user);

	while($row_user=mysqli_fetch_array($run_user)){

		$g_id = $row_user['gid'];
		$grp_name = $row_user['grp_name'];
		$grp_info = $row_user['grp_info'];
		$grp_img = $row_user['grp_img'];
		$cre = $row_user['date'];
		$user = $row_user['user_id'];
		//now displaying all at once

		echo "
		<div class='row'>
			<div class='col-sm-3'>
			</div>

			<div class='col-sm-6'>

			<div class='row' id='find_people'>
			<div class='col-sm-4'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='group_homepage.php?u_id=$user&g_n=$grp_name'>
			<img class='img-circle' src='groups_profile/$grp_img' width='150px' height='140px' title='$grp_name' style='float:left; margin:1px;'/>
			</a>
			</div>
			<div class='col-sm-6'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='group_homepage.php?u_id=$user&g_n=$grp_name'>
			<strong><h2>$grp_name</h2></strong>
			</a>
			<h3><strong><i>$grp_info</i></strong></h3>
			<h4><small style='color:black;'>Created on <strong>$cre</strong></small></h4>
			</div>
			<div class='col-sm-3'>
			</div>

			</div>

			</div>
			<div class='col-sm-4'>
			</div>
		</div><br>
		";

	}

	}


function search_member(){

	global $con;

	$use = $_SESSION['user_email'];
	$get_user = "select * from users where user_email ='$use'";
	$run_user = mysqli_query($con, $get_user);
	$row = mysqli_fetch_array($run_user);
			
	$ui_id = $row['user_id'];
	
	$a = array($ui_id);
	

	
	if(isset($_GET['search_user_btn'])){
	$search_query = htmlentities($_GET['search_user']);
	$get_user = "create table tmp as select * from (select * from users where f_name like '%$search_query%' OR l_name like '%$search_query%' OR user_name like '%$search_query%') t inner join (select * from viewfriends where profile_id = '$ui_id') k on t.user_id = k.friend_id";

	}
	else{
	$get_user = "create table tmp as select distinct * from users, viewfriends where profile_id = '$ui_id' AND user_id = friend_id";
	}

	$run_user = mysqli_query($con,$get_user);

	/*while($row_user=mysqli_fetch_array($run_user)){

		$user_id = $row_user['user_id'];
		$f_name = $row_user['f_name'];
		$l_name = $row_user['l_name'];
		$username = $row_user['user_name'];
		$user_image = $row_user['user_image'];

		$q = "insert into tmp (user_id, f_name, l_name, user_name, user_image) values ('$user_id','$f_name', '$l_name','$username','$user_image')";
		//now displaying all at once

		echo "
		<div class='row'>
			<div class='col-sm-3'>
			</div>

			<div class='col-sm-6'>

			<div class='row' id='find_people'>
			<div class='col-sm-4'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>
			<img class='img-circle' src='users/$user_image' width='150px' height='140px' title='$username' style='float:left; margin:1px;'/>
			</a>
			</div><br><br>
			<div class='col-sm-6'>
			<a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>
			<strong><h2>$f_name $l_name</h2></strong>
			</a>
			<form method='post'>
			<input type='checkbox' name='add' value='Bike'> I have a bike<br>
			<input type='hidden' name='frnd' value='$user_id' />
</form>

			</div>
			<div class='col-sm-3'>
			</div>

			</div>

			</div>
			<div class='col-sm-4'>
			</div>
		</div><br>
		";
				

	}*/
	}

function insertgpost($gid, $g_name){
	if(isset($_POST['sub1'])){
		global $con;
		global $user_id;

		$content = htmlentities($_POST['content']);
		$upload_image = $_FILES['upload_image']['name'];
		$image_tmp = $_FILES['upload_image']['tmp_name'];
		$random_number = rand(1, 999999);

		if(strlen($content) > 250){
			echo "<script>alert('Please Use 250 or less than 250 words!')</script>";
			echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name', '_self')</script>";
		}else{
			if(strlen($upload_image) >= 1 && strlen($content) >= 1){
				move_uploaded_file($image_tmp, "grouppost/$upload_image.$random_number");
				$insert = "insert into gposts (gid, user_id, post_content, post_image, post_date) values('$gid','$user_id', '$content', '$upload_image.$random_number', NOW())";

				$run = mysqli_query($con, $insert);

				if($run){
					echo "<script>alert('Your Post updated a moment ago!')</script>";
					echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name', '_self')</script>";

					
				}

				exit();
			}
			else{
				if($upload_image=='' && $content == ''){
					echo "<script>alert('Error Occured while uploading!')</script>";
					echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name', '_self')</script>";
				}else{
					if($content==''){
						move_uploaded_file($image_tmp, "grouppost/$upload_image.$random_number");
						$insert = "insert into gposts (gid, user_id,post_content,post_image,post_date) values ('$gid','$user_id','No','$upload_image.$random_number',NOW())";
						$run = mysqli_query($con, $insert);
						echo mysqli_error($con);
						if($run){
							echo "<script>alert('Your Post updated a moment ago!')</script>";
							echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name', '_self')</script>";

		
						}

						exit();
					}
					else{
					
						$insert = "insert into gposts (gid, user_id, post_content, post_date) values('$gid','$user_id', '$content', NOW())";
						$run = mysqli_query($con, $insert);

						if($run){
							echo "<script>alert('Your comment updated a moment ago!')</script>";
							echo "<script>window.open('group_homepage.php?u_id=$user_id&g_n=$g_name', '_self')</script>";

		
						}
					}
				}
			}
		}
	}
}

function show_grpposts($gid){
	global $con;

	$per_page=4;

	if (isset($_GET['page'])) {
	$page = $_GET['page'];
	}
	else {
	$page=1;
	}
	$start_from = ($page-1) * $per_page;

	$get_posts = "select * from gposts where gid ='$gid' ORDER by 1 DESC LIMIT $start_from, $per_page";

	$run_posts = mysqli_query($con,$get_posts);

	while($row_posts=mysqli_fetch_array($run_posts)){

		$post_id = $row_posts['gpost_id'];
		$g_id = $row_posts['gid'];
		$user_id = $row_posts['user_id'];
		$content = substr($row_posts['post_content'],0,40);
		$upload_image = $row_posts['post_image'];
		$post_date = $row_posts['post_date'];

		//getting the user who has posted the thread
		$user = "select * from users where user_id='$user_id'";
		$run_user = mysqli_query($con,$user);
		$row_user=mysqli_fetch_array($run_user);

		$user_name = $row_user['user_name'];
		$user_image = $row_user['user_image'];

		//now displaying all at once
		if($content=="No" && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<img id='posts-img' src='grouppost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else if(strlen($content) >= 1 && strlen($upload_image) >= 1){

			echo "
			<div class='row'>
				<div class='col-sm-3'>
				</div>
				<div id='posts' class='col-sm-6'>
				<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-12'>
						<p>$content</p>
						<img id='posts-img' src='grouppost/$upload_image' style='height:350px;'>
					</div>
				</div><br>
				
				</div>
				<div class='col-sm-3'>
				</div>
			</div><br><br>
			";

		}
		else{

		echo "

		<div class='row'>
			<div class='col-sm-3'>
			</div>
			<div id='posts' class='col-sm-6'>
			<div class='row'>
					<div class='col-sm-2'>
						<p><img src='users/$user_image' class='img-circle' width='90px' height='90px'></p>
					</div>
					<div class='col-sm-6'>
						<h3><a style='text-decoration: none;cursor: pointer;color: #3897f0;' href='user_profile.php?u_id=$user_id'>$user_name</a></h3>
						<h4><small style='color:black;'>Updated a post on <strong>$post_date</strong></small></h4>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
				<div class='row'>
					<div class='col-sm-2'>
					</div>
					<div class='col-sm-6'>
						<h3><p>$content</p></h3>
					</div>
					<div class='col-sm-4'>

					</div>
				</div>
			
			</div>
			<div class='col-sm-3'>
			</div>
		</div><br><br>

		";
	}

	}
	include("pagination.php");

}



?>

