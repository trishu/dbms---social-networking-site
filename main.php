<html>
	<head>
		<title>Trial</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	</head>
	<style>
		body{
			overflow-x:hidden;
		}
		img{
			  position:relative;
			  display: block;
 			  margin-left: auto;
  			  margin-right: auto;
  			  right:130px;
  			  width:120px;
  			  height:120px;
		}
		#centered1{
			position:absolute;
			font-size:10px;
			top:30%;
			left:30%;
			transform:translate(-50%,-50%);
		}
		#centered2{
			position:absolute;
			font-size:10px;
			top:50%;
			left:40%;
			transform:translate(-50%,-50%);
		}
		#centered3{
			position:absolute;
			font-size:10px;
			top:70%;
			left:30%;
			transform:translate(-50%,-50%);
		}	
		#signup{
			width: 60%;
			border-radius: 30px;
		}
		#login{
			width:60%;
			border-radius:30px;
			background-color:#fff;
			border:1px solid #1da1f2;
			color:#1da1f2;
		}
		#login:hover{
			width:60%;
			border-radius:30px;
			background-color:#fafeba;
			border:1px solid #1da1f2;
			color:#1da1f2;
		}
		.well{
			background-color:#187fab;
		}
	</style>
	<body>
		<div class="row">
			<div class="col-sm-12">
				<div class="well">
					<center><h1 style="color:white;">Trial Network!</h1></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class = "col-sm-4">
			</div>
			<div class="col-sm-6">
				<img src = "images/logo.jpg" alt="Social-Network"  class="img-rounded" title="timepass" >
				<h2><strong>See The Real World <br> Join Today</strong></h2><br><br>
				<h4><strong>Enjoy your Life!!</strong></h4>
				<form method="post" action="">
					<button id="signup" class="btn btn-info btn-lg" name="signup">SignUp</button><br><br>
					<?php
						if(isset($_POST['signup'])){
							echo "<script>window.open('signup.php','_self')</script>";
						}
					?>
					<button id="login" class="btn btn-info btn-lg" name="login">Login</button><br><br>
					<?php
						if(isset($_POST['login'])){
							echo "<script>window.open('signin.php','_self')</script>";
						}
					?>

				</form>
			</div>
			<div class="col-sm-4">
			</div>
		</div>
	</body>
</html>
