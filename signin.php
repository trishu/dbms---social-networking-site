<html>
	<head>
		<title>Signin</title>
		<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	</head>
	<style>
	body{
		overflow-x:hidden;
	}
	.main-content{
		width:50%;
		height:50%;
		margin:10px auto;
		background-color: #fff;
		border: 2px solid #e6e6e6;
		padding: 40px 50px;
	}
	.header{
		border:0px solid #000;
		margin-bottom:5px;
	}
	.well{
		background-color: #187fab;
	}
	#signin{
		width:60%:
		border-radius:30px;
	}
	.overlap-text{
		position:relative;
		
	}
	.overlap-text a{
		position:absolute;
		top:8px;
		right:10px;
		font-size:14px;
		text-decoration:none;
		font-family:'Overpass Mono',monospace;
		letter-spacing:-1px;
	 
	 }
	</style>
	<body>
		<div class="row">
			<div class="col-sm-12">
				<div class="well">
					<center><h1 style="color:white;">Trial Network!</h1></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="main-content">
					<div class="header">
						<h3 style="text-align:center;"><strong>Login Guys!!</strong></h3><br>
					</div>
					<div class="l-part">
						<form method="post" action ="">
							<input type="email" name="email" placeholder="Email" required="required" class="form-control input-md"><br>
						<div class="overlap-text">
							<input type="password" name="pass" placeholder="password" required="required" class="form-control input-md"><br>
						<a href = "forgot_password.php" style="text-decoration:none;float:right;color:#187fab;" data-toogle="tooltip" title="reset password">forget password</a>
						</div>
							<a href = "signup.php" style="text-decoration:none;float:right;color:#187fab;" data-toogle="tooltip" title="create account">Don't have account</a><br>
							<center><button id="signin" class="btn btn-info btn-lg" name="login">Login</button></center>
						<?php include('login.php') ?>
	
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
